<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/send', function () {
    return [
        'success' => true,
        'message' => 'Message sent'
    ];
})->name('send');


Route::get('/{url}', function () {
    return view('app');
})->name('app')->where('url','.*');

