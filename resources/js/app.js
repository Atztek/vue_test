
require('./bootstrap');

window.Vue = require('vue');


import Vuetify from "vuetify";
import VueRouter from "vue-router";
import VueCarousel from "vue-carousel";

Vue.use(VueCarousel);
Vue.use(VueRouter);
Vue.use(Vuetify);

import Home from './components/home'
import About from "./components/about";
import Contact from "./components/contact";

import store from "./store";

const routes = [
    { path: "/", component: Home, name: "home" },
    { path: "/about/", component: About, name: "about" },
    { path: "/contact/", component: Contact, name: "contact" }
];

const router = new VueRouter({
    mode: "history",
    routes 
});

const app = new Vue({
    router,
    store,
    el: "#app",
    data: {
        toolbarOriginTop: false,
        sticky: false
    },
    mounted: function() {
        window.scroll(0, 0);
    },
    computed: {
        fakeToolbarStyles: function() {
            return {
                height: (this.sticky)?this.$refs.stickToolbar.computedContentHeight + "px": 0+"px"
            };
        }
    },
    methods: {
        onScroll: function() {
            if (this.toolbarOriginTop !== false){
                let offsetTop =
                    window.pageYOffset || document.documentElement.scrollTop;

                this.sticky = offsetTop >= this.toolbarOriginTop;
            }
        },
        onResize: function() {
            this.toolbarOriginTop = this.$refs.fakeToolbar.offsetTop;
        }
    }
});
