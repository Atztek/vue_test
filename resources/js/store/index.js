import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        contactForm: {
            name: '',
            phone: '',
            message: ''
        }
    },
    getters: {
        contactForm: state => {
            return state.contactForm;
        }
    },
    mutations: {
        setContact(state, formData) {
            state.contactForm = formData;
        }
    }
});
