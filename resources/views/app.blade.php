<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Test assignment</title>
        <link rel="stylesheet" href="/css/app.css"/>
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body>
        <div id='app'>
            <v-app
            v-scroll="onScroll"
            v-resize="onResize"
            >
                <div class='blue darken-1 white--text'>
                    <v-container>
                        <p class='display-3'>Lorem.</p>
                        <v-divider></v-divider>
                        <p class="headline">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                        aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                        Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                        occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                        </p>
                    </v-container>
                </div>
                <div class="fake-toolbar" ref='fakeToolbar' :style="fakeToolbarStyles"></div>
                <v-toolbar 
                app 
                color='purple darken-3' 
                dark
                :fixed='sticky'
                :class='{"v-toolbar--sticky": !sticky}'
                ref='stickToolbar'
                :scroll-threshold='200'
                >
                    <v-toolbar-title class="hidden-sm-and-down">Test Assignment</v-toolbar-title>
                    <v-spacer></v-spacer>
                    <v-toolbar-items>
                        <v-btn flat :to='{name: "home"}' exact>Home</v-btn>
                        <v-btn flat :to='{name: "about"}' exact>About Us</v-btn>
                        <v-btn flat :to='{name: "contact"}' exact>Contact Us</v-btn>
                    </v-toolbar-items>
                </v-toolbar>
                
                <v-content class="main-content-fake-height">
                    <v-container >
                        <router-view></router-view>
                    </v-container>
                </v-content>
            </v-app>
        </div>
        <script src="/js/app.js"></script>
    </body>
</html>
